using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace Jwt.Client;

public class Program
{
    public static async Task Main(string[] args)
    {
        var builder = WebAssemblyHostBuilder.CreateDefault(args);

        string apiServer = builder.HostEnvironment.IsDevelopment() ? "https://localhost:7060" : "https://localhost:7060";


        builder.RootComponents.Add<App>("#app");
        builder.RootComponents.Add<HeadOutlet>("head::after");
        builder.Services.AddAuthorizationCore();
        builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(apiServer) });
        builder.Services.AddLocalStorageServices();
        builder.Services.AddScoped<AuthenticationStateProvider, ApiAuthenticationStateProvider>();
        builder.Services.AddScoped<ApiService>();

        await builder.Build().RunAsync();
    }
}
